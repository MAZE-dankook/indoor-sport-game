#!/bin/bash

BACK_CR="\033[0m"
RED_CR="\033[0;31m"
GREEN_CR="\033[0;32m"

export THIRD_PARTY_PATH="*/third_party/*"
export OUT_PATH="*/out/*"
export IMAGE_PATH="*/image/*"

export WEB_ROOT_PATH="*/frontend/*"
export WEB_STATIC_PATH="*/static/*"
export WEB_TEMPLATES_PATH="*/templates/*"

export BACKEND_VIRTUAL_ENV_PATH="*/.env*/*"

export SERVER_KEY_PATH="*/key/*"

export DB_BACKUP_FILE="*.sql"

export TAG_FILE="tags"
export CSCOPE_FILE="cscope.*"


echo_func()
{
	if [[ $2 -eq 1 ]]
	then
		echo -e "${RED_CR}${1}${BACK_CR}"
	else
		echo -e "${GREEN_CR}${1}${BACK_CR}"
	fi
}

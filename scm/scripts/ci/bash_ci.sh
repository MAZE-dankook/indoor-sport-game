#!/bin/bash
# shellcheck source=/dev/null

export ENV SHELLCHECK_OPTS="--shell=bash --exclude=SC1091"

set -e

ROOT_DIR=$(git rev-parse --show-toplevel)
source "${ROOT_DIR}/scm/scripts/common/echo.sh"

cd "${ROOT_DIR}"
pwd

RESULT=$(find . \
	-not -path './.git/*' -a \
	-not -path './gentags.sh' -a \
	-not -path "${THIRD_PARTY_PATH}" -a \
	-not -path "${OUT_PATH}" -a \
	-not -path "${IMAGE_PATH}" -a \
	-not -path "${WEB_ROOT_PATH}" -a \
	-not -path "${WEB_STATIC_PATH}" -a \
	-not -path "${WEB_TEMPLATES_PATH}" -a \
	-not -path "${BACKEND_VIRTUAL_ENV_PATH}" -a \
	-not -path "${SERVER_KEY_PATH}" \
	! -name "${DB_BACKUP_FILE}" \
	-name "*.sh")
BASH_LISTS=$(echo "${RESULT}" | tr '\n' ' ')

for SCRIPT in ${BASH_LISTS}
do
	echo "Checking the ${SCRIPT}"
	shellcheck -x "${SCRIPT}"

	RET="$?"
	if [[ "${RET}" != "0" ]]
	then
		echo_func "[SCM ERR] Need to check the ${SCRIPT} file"
		exit 1
	fi
done

echo_func "[scm] Bash scripts CI test done!" 0
